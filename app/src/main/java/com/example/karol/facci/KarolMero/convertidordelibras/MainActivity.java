package com.example.karol.facci.KarolMero.convertidordelibras;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button convertir;
    EditText ingreso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        convertir = (Button) findViewById(R.id.btnconvertir);

        ingreso = (EditText) findViewById(R.id.txtingreso);

        convertir.setOnClickListener(this);


    }
    @Override
    public void onClick(View view) {

     switch (view.getId()){
         case R.id.btnconvertir:

        if (ingreso.getText().toString().equals("")){
            Toast.makeText(MainActivity.this,"NO DEJAR VACIO" , Toast.LENGTH_LONG).show();
            ingreso.requestFocus();
        }
        else {
            String Ingresar = ingreso.getText().toString();
            Intent intent = new Intent(MainActivity.this, Main2Activity.class);
            intent.putExtra("ingresar", Ingresar);
            startActivity(intent);
            ingreso.setText("");

        }
        break;
        }
    }
}
