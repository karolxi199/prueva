package com.example.karol.facci.KarolMero.convertidordelibras;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    TextView Conversor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Conversor =(TextView)findViewById(R.id.btnconversion);

        Double Libras = Double.valueOf(getIntent().getStringExtra("Libras"));

        Double Resultado = (Libras)/0.0022046;

        Conversor.setText(getString(R.string.Respuesta) +" " + Resultado + getString(R.string.librass));
    }
}
